<?php

namespace Drupal\easy_migration;

use Drupal\taxonomy\Entity\Term;

/**
 * Implements Easy Migration Taxonomy Term trait.
 */
trait EasyMigrateTaxonomyTrait {

  /**
   * Get an array of the previously migrated taxonomy terms.
   *
   * @param string $tids_separated_by_commas
   *   The origin ids separated by comma (,).
   *
   * @return array
   *   Array of taxonomy terms.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getTaxonomies(string|NULL $tids_separated_by_commas) : array {
    $terms = [];

    if ($tids_separated_by_commas) {
      $tids = explode(',', $tids_separated_by_commas);

      if (!count($tids)) {
        return [];
      }

      foreach ($tids as $tid) {
        $term = $this->getMigratedEntity($tid, 'taxonomy_term');
        $terms[] = $term;
      }
    }

    return $terms;
  }

}
