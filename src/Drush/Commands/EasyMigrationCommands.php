<?php

namespace Drupal\easy_migration\Drush\Commands;

use Consolidation\OutputFormatters\StructuredData\RowsOfFields;
use Drupal\easy_migration\EntityMigrationPluginManager;
use Drush\Attributes as CLI;
use Drush\Commands\DrushCommands;
use Drush\Exceptions\UserAbortException;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * The Drush Easy Migration drush commandfile.
 */
final class EasyMigrationCommands extends DrushCommands {

  /**
   * Constructs an EasyMigrationCommands object.
   */
  public function __construct(
    private readonly EntityMigrationPluginManager $pluginManager,
  ) {
    parent::__construct();
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('plugin.manager.easy_migration.entity_migration_plugin_manager'),
    );
  }

  /**
   * Easy Migration status.
   */
  #[CLI\Command(name: 'easy_migration:status', aliases: ['ems'])]
  #[CLI\Usage(name: 'easy_migration:status', description: 'Provide the current Easy Migration status')]
  #[CLI\FieldLabels(labels: [
    'order' => 'Order',
    'id' => 'ID',
    'label' => 'Label',
    'description' => 'Description',
    'total' => 'Total',
    'migrated' => 'Migrated',
  ])]
  #[CLI\DefaultTableFields(fields: [
    'order',
    'id',
    'label',
    'description',
    'total',
    'migrated',
  ])]
  #[CLI\FilterDefaultField(field: 'order')]
  public function status($options = ['format' => 'table']): RowsOfFields {
    $plugin_definitions = $this->pluginManager->getDefinitions();
    usort($plugin_definitions, function ($a, $b) {
      return (int) $a['order'] > (int) $b['order'];
    });

    $rows = [];
    foreach ($plugin_definitions as $definition) {
      /** @var \Drupal\easy_migration\EntityMigrationBase $instance */
      $instance = $this->pluginManager->createInstance($definition['id']);
      $rows[] = [
        'order' => $definition['order'],
        'id' => $definition['id'],
        'label' => $definition['label'],
        'description' => $definition['description'],
        'total' => $instance->countItemsToMigrate(),
        'migrated' => $instance->countMigratedItems(),
      ];
    }

    return new RowsOfFields($rows);
  }

  /**
   * Migrate content using the Easy Migration module.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  #[CLI\Command(name: 'easy_migration:migrate', aliases: ['emi'])]
  #[CLI\Option(name: 'id', description: 'The Easy Migration plugin id')]
  #[CLI\Option(name: 'tag', description: 'The Easy Migration tag')]
  #[CLI\Usage(name: 'easy_migration:migrate --id=page', description: 'Migrate all content for the plugin id equal to "page"')]
  #[CLI\Usage(name: 'easy_migration:migrate --tag=node,user', description: 'Migrate all content where the plugin tags has "node" OR "user".')]
  public function migrate(array $options = [
    'id' => '',
    'tags' => '',
  ]) {

    $id = $options['id'];
    $tags = $options['tags'] ? explode(',', $options['tags']) : [];

    if ($id) {
      $plugin_definitions = [$this->pluginManager->getDefinition($id)];
    }
    else {
      $plugin_definitions = $this->pluginManager->getDefinitions();
    }

    usort($plugin_definitions, function ($a, $b) {
      return (int) $a['order'] > (int) $b['order'];
    });

    foreach ($plugin_definitions as $definition) {
      $instance = $this->pluginManager->createInstance($definition['id']);
      $do_migrate = TRUE;
      if (count($tags)) {
        $plugin_tags = $definition['tags'];
        $tags_intersect = array_intersect($tags, $plugin_tags);
        $do_migrate = !empty($tags_intersect);
      }

      if ($do_migrate) {
        $instance->doMigrate();
      }
    }
  }

  /**
   * Rolls back an Easy Migration plugin.
   */
  #[CLI\Command(name: 'easy_migration:rollback', aliases: ['emrollback'])]
  #[CLI\Option(name: 'id', description: 'The Easy Migration plugin id')]
  #[CLI\Option(name: 'tag', description: 'The Easy Migration tag')]
  #[CLI\Usage(name: 'drush emrollback', description: 'Rolls back all easy migration')]
  #[CLI\Usage(name: 'drush emrollback --id=page', description: 'Rolls back all content for the plugin id equal to "page"')]
  #[CLI\Usage(name: 'drush emrollback --tag=node,user', description: 'Rolls back content where the plugin tags has "node" OR "user".')]
  public function rollback(array $options = [
    'id' => '',
    'tags' => '',
  ]) {
    $confirm = $this->io()->confirm('Do you really want to rollback the easy migration?', FALSE);
    if (!$confirm) {
      throw new UserAbortException('Easy Migration Rollback cancelled.');
    }

    $id = $options['id'];
    $tags = $options['tags'] ? explode(',', $options['tags']) : [];

    if ($id) {
      $plugin_definitions = [$this->pluginManager->getDefinition($id)];
    }
    else {
      $plugin_definitions = $this->pluginManager->getDefinitions();
    }

    // Use the revert order.
    usort($plugin_definitions, function ($a, $b) {
      return (int) $a['order'] < (int) $b['order'];
    });

    foreach ($plugin_definitions as $definition) {
      /** @var \Drupal\easy_migration\EntityMigrationBase $instance */
      $instance = $this->pluginManager->createInstance($definition['id']);
      $do_rollback = TRUE;
      if (count($tags)) {
        $plugin_tags = $definition['tags'];
        $tags_intersect = array_intersect($tags, $plugin_tags);
        $do_rollback = !empty($tags_intersect);
      }

      if ($do_rollback) {
        $instance->rollback();
      }
    }

  }

}
