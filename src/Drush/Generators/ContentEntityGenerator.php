<?php

namespace Drupal\easy_migration\Drush\Generators;

use DrupalCodeGenerator\Asset\AssetCollection as Assets;
use DrupalCodeGenerator\Attribute\Generator;
use DrupalCodeGenerator\Command\BaseGenerator;
use DrupalCodeGenerator\GeneratorType;

/**
 * Implements the Easy Migration code generator for Content Entity.
 */
#[Generator(
  name: 'plugin:easy_migration:content_entity',
  description: 'Generates a content entity migration service for Easy Migration.',
  aliases: ['em-content_entity'],
  templatePath: __DIR__ . '/../../../templates/generator',
  type: GeneratorType::MODULE_COMPONENT,
)]
final class ContentEntityGenerator extends BaseGenerator {

  /**
   * {@inheritdoc}
   */
  protected function generate(array &$vars, Assets $assets): void {
    $ir = $this->createInterviewer($vars);

    $vars['machine_name'] = $ir->askMachineName();
    $vars['name'] = $ir->askName();
    $vars['class'] = $ir->askClass(default: '{machine_name|camelize}');

    $assets->addFile('src/Plugin/EasyMigration/{class}.php', 'content-entity.twig');
  }

}
