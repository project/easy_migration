<?php

namespace Drupal\easy_migration;

use Drupal\Component\Plugin\PluginInspectionInterface;
use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityInterface;
use Drupal\file\Entity\File;

/**
 * Defines de interface for the Entity Migration Plugin.
 */
interface EntityMigrationPluginInterface extends PluginInspectionInterface {

  /**
   * Get all IDs related to the entities to be migrated.
   *
   * On this step, our plugin will return a single column array including all
   * IDs related to the entity to be migrated. This step is important to prevent
   * the usage of too much memory during the extraction process.
   *
   * @return array
   *   The array of entity ids to be migrated.
   */
  public function getIds(): array;

  /**
   * Get the data array that contains all data from a single entity to migrate.
   *
   * @return array
   *   The entity data array to be migrated.
   */
  public function getData($entity_id): array;

  /**
   * Create or update an entity.
   *
   * @param array $data
   *   The data array.
   *
   * @return \Drupal\Core\Entity\ContentEntityBase|NULL
   *   The created entity.
   */
  public function saveEntity(array $data): ContentEntityBase | NULL;

  /**
   * Execute the migration.
   */
  public function doMigrate() : void;

  /**
   * Rollback the plugin migration.
   *
   * As each plugin has its own implementation, this function should be
   * implemented in the plugin implementation class.
   *
   * @return string
   *   The message provided by the rollback function.
   */
  public function rollback();

  /**
   * Get the migrated entity id.
   *
   * @param int $entity_id_origin
   *   The origin entity id.
   * @param string $entity_type
   *   The destination entity type.
   *
   * @return int
   *   The corresponding imported entity id. Zero (0) if the entity has not been
   *   already migrated.
   */
  public function getMigratedEntityId(int $entity_id_origin, string $entity_type): int;

  /**
   * Get the Easy Migration plugin name.
   *
   * @return string
   *   The plugin label.
   */
  public function getLabel() : string;

  /**
   * Get the Easy Migration plugin main entity type.
   *
   * @return string
   *   The main entity type handled by the plugin.
   */
  public function getEntityType() : string;

  /**
   * Get the Easy Migration plugin execution order.
   *
   * @return string
   *   The plugin execution order.
   */
  public function getOrder() : int;

  /**
   * Get the Easy Migration plugin tags.
   *
   * @return array
   *   The plugin tags
   */
  public function getTags() : array;

  /**
   * Get the source name from the plugin definition.
   *
   * @return string
   *   The plugin data source.
   */
  public function getSource() : string;

  /**
   * Get the Easy Migration plugin description.
   *
   * @return string
   *   The plugin description.
   */
  public function getDescription() : string;

  /**
   * Verify if a given entity has already been migrated or not.
   *
   * @param int $entity_id_origin
   *   The origin entity id.
   * @param string $entity_type
   *   The destination entity type.
   *
   * @return bool
   *   Whether the entity has already been migrated or not.
   */
  public function isAlreadyMigrated(int $entity_id_origin, string $entity_type): bool;

  /**
   * Get the migrated entity.
   *
   * @param int $entity_id_origin
   *   The origin entity id.
   * @param string $entity_type
   *   The destination entity type.
   *
   * @return \Drupal\Core\Entity\EntityInterface|null
   *   The migrated entity object, or NULL if not found.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getMigratedEntity(int $entity_id_origin, string $entity_type): EntityInterface|null;

  /**
   * Set a given entity as Referenced entity in the easy_migration table.
   *
   * @param int $entity_id_origin
   *   The origin entity id.
   * @param string $entity_type
   *   The destination entity type.
   */
  public function setMigratedEntityAsReferenced(int $entity_id_origin, string $entity_type) : void;

  /**
   * Insert a new migration item log if applicable.
   *
   * @param int $entity_id_origin
   *   The origin entity id.
   * @param int $entity_id_new
   *   The new entity id.
   * @param string $entity_type
   *   The destination entity type.
   * @param string $uuid
   *   The entity destination UUID.
   * @param bool $is_referenced
   *   If the entity is being referred by another entity.
   * @param array $log
   *   Custom log array to store information you might need.
   *
   * @throws \Exception
   */
  public function updateEasyMigrationLogTable(int $entity_id_origin, int $entity_id_new, string $entity_type, string $uuid, bool $is_referenced = FALSE, array $log = []) : void;

  /**
   * Get the easy_migration migration log.
   *
   * @param string|null $plugin_id
   *   The plugin id responsible for the log entry.
   * @param string|null $entity_type
   *   The migrated entity type.
   * @param int|null $entity_id_origin
   *   The origin entity id.
   * @param int|null $entity_id_new
   *   The migrated entity id.
   * @param string|null $uuid
   *   The migrated entity uuid.
   *
   * @return array
   *   The log array.
   */
  public function getMigrationLog(string $plugin_id = NULL, string $entity_type = NULL, int $entity_id_origin = NULL, int $entity_id_new = NULL, string $uuid = NULL) : array;

  /**
   * Get the path alias from a Drupal 7 origin database.
   *
   * @param int $entity_id_origin
   *   The origin entity id.
   * @param string|null $database_name
   *   The database name specified on your settings.local.php.
   *
   * @return string|null
   *   The path alias or NULL if not found.
   */
  public function getNodePathAliasFromDrupal7(int $entity_id_origin, string $database_name = NULL) : string|NULL;

  /**
   * Returns the number of items to be migrated.
   *
   * @return int
   *   The number of items to be migrated.
   */
  public function countItemsToMigrate() : int;

  /**
   * Returns the number of migrated items.
   *
   * @return int
   *   The number of migrated items.
   */
  public function countMigratedItems() : int;

}
