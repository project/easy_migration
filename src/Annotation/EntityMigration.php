<?php

namespace Drupal\easy_migration\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines an EntityMigration annotation object.
 *
 * @see \Drupal\easy_migration\EntityMigrationPluginInterface
 * @see \Drupal\easy_migration\EntityMigrationPluginManager
 * @see plugin_api
 *
 * @Annotation
 */
class EntityMigration extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The human-readable name of the entity migration.
   *
   * @var string
   */
  public $label;

  /**
   * The main entity type to be migrated.
   *
   * There are some cases in where a plugin can migrate multiple entity types,
   * such as a node migration that also migrates its media images as media
   * entities. In this case, use "node" as the entity type.
   *
   * @var string
   */
  public string $entity_type;

  /**
   * The order in where the migration will occur.
   *
   * It's important when we have entities referring other entities. The
   * referenced entities MUST be migrated before the entities that reference
   * them.
   *
   * @var int
   */
  public int $order = 1000;

  /**
   * The Easy Migration tags.
   *
   * Use tags to run only specific migrations.
   *
   * @var array
   */
  public array $tags = [];

  /**
   * The string to indicate the data source for the entity.
   *
   * This is optional, but fundamental in case you have multiple data sources.
   *
   * @var string
   */
  public string $source = '';

  /**
   * A brief description of the entity migration.
   *
   * This property is optional, and it does not need to be declared.
   *
   * @var string
   */
  public string $description = '';

}
