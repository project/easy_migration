<?php

namespace Drupal\easy_migration;

use Drupal\Core\Database\Connection;
use Drupal\Core\Database\Database;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Plugin\PluginBase;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Output\ConsoleOutput;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines the EntityMigration plugin base class.
 */
abstract class EntityMigrationBase extends PluginBase implements EntityMigrationPluginInterface, ContainerFactoryPluginInterface {

  use EasyMigrationFileTrait;

  /**
   * The database connection service.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected Connection $database;

  /**
   * The EntityTypeManager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, Connection $database, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->database = $database;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('database'),
      $container->get('entity_type.manager'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function doMigrate() : void {
    $ids = $this->getIds();

    // Check if the function is being called via drush command.
    $is_drush = FALSE;
    if (isset($GLOBALS['argv']) && !empty($GLOBALS['argv'][0]) && str_ends_with($GLOBALS['argv'][0], 'drush')) {
      // Prepare the progress bar.
      $is_drush = TRUE;
      $output = new ConsoleOutput();
      $progress = new ProgressBar($output, count($ids));
      $title = 'Easy Migration: ' . $this->getPluginId() . ' | ' . $this->getLabel();
      $progress->setFormat("$title\n%current%/%max% [%bar%] %percent:3s%% %elapsed:6s%/%estimated:-6s% %memory:6s%");
      $progress->start();
    }

    foreach ($ids as $id) {
      $data = $this->getData($id);
      $data = array_values($data)[0];
      $data = (array) $data;
      $entity = $this->saveEntity($data);
      if ($entity instanceof ContentEntityInterface) {
        $entity_id_new = $entity->id();
        $this->updateEasyMigrationLogTable($id, $entity_id_new, $entity->getEntityTypeId(), $entity->uuid());
      }

      if ($is_drush) {
        $progress->advance();
      }
    }

    if ($is_drush) {
      $progress->finish();
    }
  }

  /**
   * {@inheritdoc}
   */
  public function rollback() {
    $log_entries = $this->getMigrationLog($this->getPluginId());
    if (count($log_entries) <= 0) {
      return;
    }

    // Check if the function is being called via drush command.
    $is_drush = FALSE;
    if (isset($GLOBALS['argv']) && !empty($GLOBALS['argv'][0]) && str_ends_with($GLOBALS['argv'][0], 'drush')) {
      // Prepare the progress bar.
      $is_drush = TRUE;
      $output = new ConsoleOutput();
      $progress = new ProgressBar($output, count($log_entries));
      $title = 'Easy Migration - Rolling Back: ' . $this->getPluginId() . ' | ' . $this->getLabel();
      $progress->setFormat("$title\n%current%/%max% [%bar%] %percent:3s%% %elapsed:6s%/%estimated:-6s% %memory:6s%");
      $progress->start();
    }

    foreach ($log_entries as $log_entry) {
      // Delete migrated entity.
      $entity = $this->getMigratedEntity($log_entry['eid_origin'], $log_entry['entity_type']);
      $entity->delete();

      // Delete log entry.
      $this->database->delete('easy_migration')
        ->condition('eid_new', $log_entry['eid_new'])
        ->condition('eid_origin', $log_entry['eid_origin'])
        ->condition('entity_type', $log_entry['entity_type'])
        ->condition('plugin_id', $log_entry['plugin_id'])
        ->execute();

      if ($is_drush) {
        $progress->advance();
      }
    }

    if ($is_drush) {
      $progress->finish();
    }
  }

  /**
   * Get the database connection for your migration/origin database connection.
   *
   * @param string $database_name
   *   The database name specified on your settings.local.php.
   *
   * @return \Drupal\Core\Database\Connection
   *   The database connection for the origin database declared on
   *   settings.local.php.
   */
  protected function getMigrationDatabaseConnection(string $database_name = 'easy_migration'): Connection {
    Database::setActiveConnection($database_name);

    return Database::getConnection();
  }

  /**
   * {@inheritdoc}
   */
  public function getMigratedEntityId(int $entity_id_origin, string $entity_type): int {
    $result = $this->database->query("
      SELECT eid_new
        FROM {easy_migration}
       WHERE eid_origin = :eid_origin
         AND entity_type = :entity_type
         AND source_name = :source
    ", [
      ':eid_origin' => $entity_id_origin,
      ':entity_type' => $entity_type,
      ':source' => $this->getSource(),
    ])->fetchCol();

    return $result[0] ?? 0;
  }

  /**
   * {@inheritdoc}
   */
  public function getLabel() : string {
    return $this->getPluginDefinition()['label'];
  }

  /**
   * {@inheritdoc}
   */
  public function getEntityType() : string {
    return $this->getPluginDefinition()['entity_type'];
  }

  /**
   * {@inheritdoc}
   */
  public function getOrder() : int {
    return $this->getPluginDefinition()['order'];
  }

  /**
   * {@inheritdoc}
   */
  public function getTags() : array {
    return $this->getPluginDefinition()['tags'];
  }

  /**
   * {@inheritdoc}
   */
  public function getSource() : string {
    return $this->getPluginDefinition()['source'] ?? '';
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription() : string {
    return $this->getPluginDefinition()['description'];
  }

  /**
   * {@inheritdoc}
   */
  public function isAlreadyMigrated(int $entity_id_origin, string $entity_type): bool {
    return $this->getMigratedEntityId($entity_id_origin, $entity_type, $this->getSource()) != 0;
  }

  /**
   * {@inheritdoc}
   */
  public function getMigratedEntity(int $entity_id_origin, string $entity_type): EntityInterface|null {
    $id = $this->getMigratedEntityId($entity_id_origin, $entity_type, $this->getSource());

    $entity = NULL;
    if ($id) {
      $entity = $this->entityTypeManager->getStorage($entity_type)->load($id);
    }

    return $entity;
  }

  /**
   * {@inheritdoc}
   */
  public function setMigratedEntityAsReferenced(int $entity_id_origin, string $entity_type) : void {
    $query = <<<SQL
UPDATE {easy_migration}
   SET is_referenced = 1
 WHERE eid_origin = :eid_origin
   AND entity_type = :entity_type
   AND origin = :origin
SQL;

    $this->database->query($query, [
      ':eid_origin' => $entity_id_origin,
      ':entity_type' => $entity_type,
      ':origin' => $this->getSource(),
    ])->execute();
  }

  /**
   * {@inheritdoc}
   */
  public function updateEasyMigrationLogTable(int $entity_id_origin, int $entity_id_new, string $entity_type, string $uuid, bool $is_referenced = FALSE, array $log = []) : void {
    if ($this->isAlreadyMigrated($entity_id_origin, $entity_type)) {
      $this->database->update('easy_migration')->fields([
        'eid_new' => $entity_id_new,
        'uuid' => $uuid,
        'is_referenced' => $is_referenced ? 1 : 0,
        'log' => serialize($log),
      ])->condition('eid_origin', $entity_id_origin)
        ->condition('entity_type', $entity_type)
        ->condition('source_name', $this->getSource())
        ->execute();
    }
    else {
      $this->database->insert('easy_migration')->fields([
        'plugin_id' => $this->getPluginId(),
        'eid_origin' => $entity_id_origin,
        'entity_type' => $entity_type,
        'eid_new' => $entity_id_new,
        'uuid' => $uuid,
        'source_name' => $this->getSource(),
        'is_referenced' => $is_referenced ? 1 : 0,
        'log' => serialize($log),
      ])->execute();
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getMigrationLog(string $plugin_id = NULL, string $entity_type = NULL, int $entity_id_origin = NULL, int $entity_id_new = NULL, string $uuid = NULL) : array {
    $result = $this->database->select('easy_migration', 'em')
      ->fields('em', [
        'plugin_id',
        'eid_origin',
        'eid_new',
        'uuid',
        'entity_type',
        'source_name',
        'is_referenced',
        'log',
      ]);

    if ($plugin_id) {
      $result->condition('plugin_id', $plugin_id);
    }
    if ($entity_type) {
      $result->condition('entity_type', $entity_type);
    }
    if ($entity_id_origin) {
      $result->condition('eid_origin', $entity_id_origin);
    }
    if ($entity_id_new) {
      $result->condition('eid_new', $entity_id_new);
    }
    if ($uuid) {
      $result->condition('uuid', $uuid);
    }

    $logs = $result->execute()->fetchAll();
    foreach ($logs as &$log) {
      $log = (array) $log;
    }

    return $logs;
  }

  /**
   * {@inheritdoc}
   */
  public function getNodePathAliasFromDrupal7(int $entity_id_origin, string $database_name = NULL) : string|NULL {
    $connection = $this->getMigrationDatabaseConnection($database_name);
    $alias = $connection->query("
        SELECT alias
          FROM url_alias
         WHERE source = :path
    ", [':path' => "node/$entity_id_origin"])
      ->fetchCol();

    return $alias[0] ?? NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function countItemsToMigrate() : int {
    $ids = $this->getIds();

    return count($ids);
  }

  /**
   * {@inheritdoc}
   */
  public function countMigratedItems() : int {
    $query = <<<SQL
SELECT count(eid_new)
  FROM easy_migration
WHERE plugin_id = :plugin_id
  AND entity_type = :entity_type
SQL;

    $result = $this->database->query($query, [
      ':plugin_id' => $this->getPluginId(),
      ':entity_type' => $this->getEntityType(),
    ])
      ->fetchCol();

    return $result[0] ?? 0;
  }

}
