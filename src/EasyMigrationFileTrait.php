<?php

namespace Drupal\easy_migration;

use Drupal\Core\File\Exception\FileException;
use Drupal\Core\File\FileSystemInterface;
use Drupal\file\Entity\File;
use Drupal\file\FileInterface;

/**
 * Implements Easy Migration file trait.
 */
trait EasyMigrationFileTrait {

  /**
   * Copy a file from one location into Drupal filesystem.
   *
   * @param string $file_uri
   *   The file URL.
   *     - https://example.com/images/sample-01.png
   *     - /path-to-local-image/sample-01.jpg
   * @param string $destination_folder
   *   The destination directory. Use the Drupal URI standard.
   *     - public://path-to-your-public-subdirectory
   *     - private://path-to-your-private-subdirectory
   * @param string|null $new_filename
   *   The new filename without the extension. NULL to keep the same name.
   * @param int $replace
   *   The FileSystemInterface constants:
   *     - FileSystemInterface::EXISTS_RENAME
   *     - FileSystemInterface::EXISTS_REPLACE
   *     - FileSystemInterface::EXISTS_ERROR
   *
   * @return string
   *   The destination file URI.
   *
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function copyFileFromUri(string $file_uri, string $destination_folder, string $new_filename = NULL, int $replace = FileSystemInterface::EXISTS_REPLACE) : string {
    $file_system = \Drupal::getContainer()->get('file_system');
    $file_extension = pathinfo($file_uri, PATHINFO_EXTENSION);
    $filename_original = pathinfo($file_uri, PATHINFO_FILENAME);
    $filename = $new_filename ? "$new_filename.$file_extension" : "$filename_original.$file_extension";
    $file_destination = "$destination_folder/$filename";

    // Create the directory if it doesn't exist.
    $file_system->prepareDirectory($destination_folder, FileSystemInterface::CREATE_DIRECTORY);

    // Copy the file from URL.
    if (str_starts_with($file_uri, '/')) {
      // If URI is a local folder, use Drupal file_system copy method.
      $destination = $file_system->copy($file_uri, $file_destination, $replace);
    }
    elseif (str_starts_with($file_uri, 'http://') || str_starts_with($file_uri, 'https://')) {
      // Download the file from the URL.
      $response = \Drupal::httpClient()->get($file_uri);
      if ($response->getStatusCode() == 200) {
        file_put_contents($file_destination, $response->getBody());
        $destination = $file_destination;
      }
      else {
        throw new FileException("File '$file_uri' could not be copied because it does not exist. Code: " . $response->getStatusCode());
      }
    }
    else {
      throw new FileException("File '$file_uri' must be a local file, a HTTP:// or HTTPS:// URI");
    }

    return $destination;
  }

  /**
   * Migrates a file from Drupal 7 into current Drupal installation.
   *
   * @param int $drupal_file_id
   *   The drupal file id.
   * @param string $file_base_uri
   *   The file base URI.
   * @param string|null $destination_folder
   *   The destination folder or NULL to keep the same structure as the origin.
   *
   * @return \Drupal\file\Entity\File
   *   The migrated File entity.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function migrateFileFromDrupal(int $drupal_file_id, string $file_base_uri, string|NULL $destination_folder = NULL, string $database_name = 'easy_migration') : File {
    $query = <<<SQL
SELECT fid
     , uid
     , filename
     , uri
     , filemime
     , filesize
     , status
 FROM file_managed
WHERE fid = :fid;
SQL;

    $connection = $this->getMigrationDatabaseConnection($database_name);
    $data = $connection->query($query, [':fid' => $drupal_file_id])->fetchAll();
    $data = (array) array_values($data)[0];

    if (stristr($data['uri'], 'private://')) {
      $file_path = str_replace('private://', '', $data['uri']);
    }
    else {
      $file_path = str_replace('public://', '', $data['uri']);
    }
    $file_origin = $file_base_uri . '/' . $file_path;
    $arr = explode('/', $data['uri']);
    array_pop($arr);
    $destination_folder = $destination_folder ?? implode('/', $arr);
    $file_new_uri = $this->copyFileFromUri($file_origin, $destination_folder);

    $is_already_migrated = $this->isAlreadyMigrated($drupal_file_id, 'file', $this->getSource());
    if ($is_already_migrated) {
      /** @var \Drupal\file\Entity\File $file */
      $file = $this->getMigratedEntity($drupal_file_id, 'file', $this->getSource());
      $file->setFileUri($file_new_uri);
    }
    else {
      $values = [
        'uri' => $file_new_uri,
        'uid' => $data['uid'],
        'status' => FileInterface::STATUS_PERMANENT,
      ];

      $file = $this->entityTypeManager
        ->getStorage('file'
        )->create($values);
    }

    $file->save();

    $this->updateEasyMigrationLogTable($drupal_file_id, $file->id(), 'file', $file->uuid());

    return $file;
  }
}
