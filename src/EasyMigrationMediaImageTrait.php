<?php

namespace Drupal\easy_migration;

use Drupal\file\Entity\File;
use Drupal\media\Entity\Media;

/**
 * Implements Easy Migration Media Image trait.
 */
trait EasyMigrationMediaImageTrait {

  /**
   * @param $entity_id_origin
   *   The origin entity id.
   * @param \Drupal\file\Entity\File $file
   *   The corresponding image file.
   * @param string $title
   *   The image title.
   * @param string $alt
   *   The image alt text.
   * @param string $media_bundle
   *   The media image bundle.
   * @param string $media_field_image_name
   *   The media image field name.
   * @param string $lang_code
   *   The lang code.
   *
   * @return \Drupal\media\Entity\Media
   *   The saved media image.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function saveMediaImage($entity_id_origin, File $file, string|NULL $title = '', string|NULL $alt = '', string $media_bundle = 'image', string $media_field_image_name = 'field_media_image', string $lang_code = 'en') : Media {
    $is_already_migrated = $this->isAlreadyMigrated($entity_id_origin, 'media');
    if ($is_already_migrated) {
      $entity = $this->getMigratedEntity($entity_id_origin, 'media');
    }
    else {
      $values = [
        'bundle' => $media_bundle,
        'langcode' => $lang_code,
      ];

      /** @var \Drupal\media\Entity\Media $media */
      $entity = \Drupal::getContainer()
        ->get('entity_type.manager')
        ->getStorage('media')
        ->create($values);
    }

    $entity->set($media_field_image_name, [
      [
        'target_id' => $file->id(),
        'title' => $title ?? $file->getFilename(),
        'alt' => $alt ?? $file->getFilename(),
      ],
    ]);

    $entity->setName($title ?? $file->getFilename());
    $entity->save();

    return $entity;
  }

}
