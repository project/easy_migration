<?php

namespace Drupal\easy_migration_example\Plugin\EasyMigration;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\easy_migration\EntityMigrationBase;
use Drupal\easy_migration\EntityMigrationPluginInterface;
use Drupal\taxonomy\Entity\Vocabulary;

/**
 * TermEntity plugin for Easy Migration.
 *
 * @EntityMigration(
 *   id = "term_tag",
 *   label = "Tags Migration",
 *   entity_type = "taxonomy_term",
 *   source = "drupal7",
 *   tags = {"term"},
 *   order = 10,
 *   description = "Migrate tag vocabulaty from Drupal 7 database.",
 * )
 */
class _010_TagTermEntity extends EntityMigrationBase implements EntityMigrationPluginInterface {

  /**
   * {@inheritdoc}
   */
  public function getIds(): array {
    $query = <<<SQL
SELECT taxonomy_term_data.tid
  FROM taxonomy_term_data INNER JOIN taxonomy_vocabulary ON taxonomy_term_data.vid = taxonomy_vocabulary.vid
 WHERE taxonomy_vocabulary.machine_name = 'tags'
 ORDER BY taxonomy_term_data.tid
SQL;

    $result = $this->getMigrationDatabaseConnection("easy_migration")
      ->query($query)
      ->fetchCol();

    return $result;
  }

  /**
   * {@inheritdoc}
   */
  public function getData($entity_id): array {
    $query = <<<SQL
SELECT tid
     , vid
     , name
     , description
     , format
     , weight
  FROM taxonomy_term_data
 WHERE tid = :tid 
SQL;

    $result = $this->getMigrationDatabaseConnection("easy_migration")
      ->query($query, [':tid' => $entity_id])
      ->fetchAll();

    return $result;
  }

  /**
   * {@inheritdoc}
   */
  public function saveEntity(array $data) : ContentEntityBase | NULL {
    // Implement here the function to save the entity based on the data
    // extracted on "getData" function.
    $entity_id_origin = (int) $data['tid'];

    $is_already_migrated = $this->isAlreadyMigrated($entity_id_origin, $this->getEntityType());
    if ($is_already_migrated) {
      $entity = $this->getMigratedEntity($entity_id_origin, $this->getEntityType());
    }
    else {
      $vocabulary = Vocabulary::load('tags');
      // Create a new vocabulary if it does not exist.
      if (!$vocabulary instanceof Vocabulary) {
        $vocabulary = Vocabulary::create(['vid' => 'tags', 'name' => 'Tags'])->save();
      }

      $values = [
        'vid' => $vocabulary->id(),
        'langcode' => 'en',
        'status' => 1,
      ];

      /** @var \Drupal\taxonomy\Entity\Term $entity */
      $entity = $this->entityTypeManager
        ->getStorage($this->getEntityType())
        ->create($values);
    }

    $entity->setName($data['name']);
    $entity->setDescription($data['description']);
    $entity->setFormat('basic_html');
    $entity->setWeight($data['weight']);

    $entity->save();

    return $entity;
  }

}
