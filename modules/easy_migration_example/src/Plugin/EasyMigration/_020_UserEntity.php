<?php

namespace Drupal\easy_migration_example\Plugin\EasyMigration;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\easy_migration\EntityMigrationBase;
use Drupal\easy_migration\EntityMigrationPluginInterface;

/**
 * UserEntity plugin for Easy Migration.
 *
 * @EntityMigration(
 *   id = "user",
 *   label = "User Migration",
 *   entity_type = "user",
 *   source = "drupal7",
 *   order = 20,
 *   tags = {"user"},
 *   description = "Migrate users from Drupal 7 database.",
 * )
 */
class _020_UserEntity extends EntityMigrationBase implements EntityMigrationPluginInterface {

  /**
   * {@inheritdoc}
   */
  public function getIds(): array {
    $query = <<<SQL
SELECT uid 
  FROM users
 WHERE status = 1
   AND uid > 1
 ORDER BY uid;
SQL;

    $result = $this->getMigrationDatabaseConnection("easy_migration")
      ->query($query)
      ->fetchCol();

    return $result;
  }

  /**
   * {@inheritdoc}
   */
  public function getData($entity_id): array {
    $query = <<<SQL
SELECT uid
     , name
     , pass
     , mail
     , theme
     , signature
     , signature_format
     , created
     , access
     , login
     , status
     , timezone
     , language
     , picture
     , init
     , data
     , changed
     , (SELECT GROUP_CONCAT(rid SEPARATOR ',')
          FROM users_roles
         WHERE uid = users.uid) AS roles
  FROM users
 WHERE uid = :uid
SQL;

    $result = $this->getMigrationDatabaseConnection("easy_migration")
      ->query($query, [':uid' => $entity_id])
      ->fetchAll();

    return $result;
  }

  /**
   * {@inheritdoc}
   */
  public function saveEntity(array $data) : ContentEntityBase | NULL {
    // Implement here the function to save the entity based on the data
    // extracted on "getData" function.
    $entity_id_origin = (int) $data['uid'];
    $database_name = 'easy_migration';

    $is_already_migrated = $this->isAlreadyMigrated($entity_id_origin, $this->getEntityType());
    $user_by_name = $this->entityTypeManager->getStorage('user')->loadByProperties(['name' => $data['name']]);
    $user_by_email = $this->entityTypeManager->getStorage('user')->loadByProperties(['mail' => $data['mail']]);
    if (count($user_by_name)) {
      $entity = array_values($user_by_name)[0];
    }
    elseif (count($user_by_email)) {
      $entity = array_values($user_by_email)[0];
    }
    elseif ($is_already_migrated) {
      $entity = $this->getMigratedEntity($entity_id_origin, $this->getEntityType());
    }
    else {
      $values = [
        'langcode' => $data['langcode'] ?? 'en',
        'name' => $data['name'],
        'pass' => $data['pass'],
        'mail' => $data['mail'],
        'timezone' => $data['timezone'],
        'status' => $data['status'],
        'access' => $data['access'],
        'login' => $data['login'],
        'init' => $data['init'],
      ];

      /** @var \Drupal\user\Entity\User $entity */
      $entity = $this->entityTypeManager
        ->getStorage($this->getEntityType())
        ->create($values);
    }

    $entity->setUsername($data['name']);
    $entity->pass = $data['pass'];
    $entity->mail = $data['mail'];
    $entity->timezone = $data['timezone'];
    $data['status'] == 1 ? $entity->activate() : $entity->block();
    $entity->created = $data['created'];
    $entity->changed = $data['changed'];
    $entity->login = $data['login'];

    // Apply user roles.
    $roles_dictionary = [
      '1' => 'anonymous',
      '2' => 'authenticated',
      '3' => 'administrator',
      '4' => 'content_editor',
    ];
    $roles = explode(',', $data['roles']);
    foreach ($roles as $role) {
      if (isset($roles_dictionary[$role])) {
        $entity->addRole($roles_dictionary[$role]);
      }
    }

    // Handler user picture.
    if ($data['picture']) {
      // If you are using a files folder.
      $file_uri = '/app/migration/files';
      // Or ad following if you are using files from a website.
      // $file_uri = 'https://example.com/sites/default/files';
      $picture = $this->migrateFileFromDrupal7($data['picture'], $database_name, $file_uri, 'public://pictures');
      $entity->set('user_picture', $picture);
    }

    $entity->save();

    return $entity;
  }

}
