<?php

namespace Drupal\easy_migration_example\Plugin\EasyMigration;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\easy_migration\EntityMigrationBase;
use Drupal\easy_migration\EntityMigrationPluginInterface;

/**
 * PageEntity plugin for Easy Migration.
 *
 * @EntityMigration(
 *   id = "article",
 *   label = "Article Migration",
 *   entity_type = "node",
 *   source = "drupal7",
 *   order = 40,
 *   tags = {"content", "node"},
 *   description = "Migrate article content from Drupal 7 database.",
 * )
 */
class _040_ArticleEntity extends EntityMigrationBase implements EntityMigrationPluginInterface {

  /**
   * {@inheritdoc}
   */
  public function getIds(): array {
    $query = <<<SQL
SELECT nid
  FROM node
 WHERE type = 'article' 
 ORDER BY nid;
SQL;

    $result = $this->getMigrationDatabaseConnection("easy_migration")
      ->query($query)
      ->fetchCol();

    return $result;
  }

  /**
   * {@inheritdoc}
   */
  public function getData($entity_id): array {
    // @todo implement here the function to grab the data from the origin source.
    $query = <<<SQL
SELECT node.nid
     , node.type
     , node.language
     , node.title
     , node.uid
     , node.status
     , node.created
     , node.changed
     , node.promote
     , node.sticky
     , field_data_body.body_value
     , field_data_body.body_summary
     , field_data_body.body_format
     , field_data_field_image.field_image_fid
     , field_data_field_image.field_image_alt
     , field_data_field_image.field_image_title
     , (SELECT GROUP_CONCAT(field_tags_tid SEPARATOR ',')
          FROM field_data_field_tags
         WHERE entity_id = node.nid) AS field_tags
  FROM node
         LEFT JOIN field_data_body ON node.nid = field_data_body.entity_id
         LEFT JOIN field_data_field_image field_data_field_image ON field_data_body.entity_id = field_data_field_image.entity_id
 WHERE nid = :nid;
SQL;

    $result = $this->getMigrationDatabaseConnection("easy_migration")
      ->query($query, [':nid' => $entity_id])
      ->fetchAll();

    return $result;
  }

  /**
   * {@inheritdoc}
   */
  public function saveEntity(array $data): ContentEntityBase | NULL {
    // Implement here the function to save the entity based on the data
    // extracted on "getData" function.
    $entity_id_origin = $data['nid'];
    $database_name = 'easy_migration';

    $is_already_migrated = $this->isAlreadyMigrated($entity_id_origin, $this->getEntityType());
    if ($is_already_migrated) {
      $entity = $this->getMigratedEntity($entity_id_origin, $this->getEntityType());
    }
    else {
      // On this example, we need to get the imported user id in order to keep
      // the authorship.
      $author = $this->getMigratedEntity($data['uid'], 'user');

      $values = [
        'type' => 'article',
        'langcode' => 'en',
        'status' => $data['status'],
        'uid' => $author ? $author->id() : 1,
        'created' => $data['created'],
      ];

      /** @var \Drupal\node\Entity\Node $entity */
      $entity = $this->entityTypeManager
        ->getStorage($this->getEntityType())
        ->create($values);
    }

    $entity->set('created', $data['created']);
    $entity->set('changed', $data['changed']);
    $entity->setTitle($data['title']);

    $entity->set('body', [
      [
        'bundle' => 'article',
        'langcode' => 'en',
        'value' => $data['body_value'],
        'summary' => strip_tags($data['body_summary']),
        'format' => 'full_html',
      ],
    ]);

    // Migrate article tags.
    if ($data['field_tags']) {
      $tags = [];
      $tids = explode(',', $data['field_tags']);
      foreach ($tids as $tid) {
        $tag = $this->getMigratedEntity($tid, 'taxonomy_term');
        $tags[] = $tag;
      }
      $entity->set('field_tags', $tags);
    }

    // Migrate article image.
    if ($data['field_image_fid']) {
      $file_uri = '/app/migration/files';
      // Or ad following if you are using files from a website.
      // $file_uri = 'https://example.com/sites/default/files';
      $destination_folder = 'public://images';
      $image = $this->migrateFileFromDrupal7($data['field_image_fid'], $database_name, $file_uri, $destination_folder);
      $entity->set('field_image', $image);
    }

    $entity->save();

    return $entity;
  }

}
