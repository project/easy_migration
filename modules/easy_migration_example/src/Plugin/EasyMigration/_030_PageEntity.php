<?php

namespace Drupal\easy_migration_example\Plugin\EasyMigration;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\easy_migration\EntityMigrationBase;
use Drupal\easy_migration\EntityMigrationPluginInterface;

/**
 * PageEntity plugin for Easy Migration.
 *
 * @EntityMigration(
 *   id = "page",
 *   label = "Page Migration",
 *   entity_type = "node",
 *   source = "drupal7",
 *   order = 30,
 *   tags = {"content", "node"},
 *   description = "Migrate page content from Drupal 7 database.",
 * )
 */
class _030_PageEntity extends EntityMigrationBase implements EntityMigrationPluginInterface {

  /**
   * {@inheritdoc}
   */
  public function getIds(): array {
    $query = <<<SQL
SELECT nid
  FROM node
 WHERE type = 'page' 
 ORDER BY nid;
SQL;

    $result = $this->getMigrationDatabaseConnection("easy_migration")
      ->query($query)
      ->fetchCol();

    return $result;
  }

  /**
   * {@inheritdoc}
   */
  public function getData($entity_id): array {
    // Implement here the function to grab the data from the origin source.
    $query = <<<SQL
SELECT node.nid
     , node.type
     , node.language
     , node.title
     , node.uid
     , node.status
     , node.created
     , node.changed
     , node.promote
     , node.sticky
     , field_data_body.body_value
     , field_data_body.body_summary
     , field_data_body.body_format
  FROM node
         LEFT JOIN field_data_body ON node.nid = field_data_body.entity_id
 WHERE nid = :nid;
SQL;

    $result = $this->getMigrationDatabaseConnection("easy_migration")
      ->query($query, [':nid' => $entity_id])
      ->fetchAll();

    return $result;
  }

  /**
   * {@inheritdoc}
   */
  public function saveEntity(array $data) : ContentEntityBase | NULL {
    // Implement here the function to save the entity based on the data
    // extracted on "getData" function.
    $entity_id_origin = $data['nid'];

    $is_already_migrated = $this->isAlreadyMigrated($entity_id_origin, $this->getEntityType());
    if ($is_already_migrated) {
      $entity = $this->getMigratedEntity($entity_id_origin, $this->getEntityType());
    }
    else {
      // On this example, we need to get the imported user id in order to keep
      // the authorship.
      $author = $this->getMigratedEntity($data['uid'], 'user');

      $values = [
        'type' => 'page',
        'langcode' => 'en',
        'status' => $data['status'],
        'uid' => $author ? $author->id() : 1,
        'created' => $data['created'],
      ];

      /** @var \Drupal\node\Entity\Node $entity */
      $entity = $this->entityTypeManager
        ->getStorage($this->getEntityType())
        ->create($values);
    }

    $entity->set('created', $data['created']);
    $entity->set('changed', $data['changed']);
    $entity->setTitle($data['title']);

    $entity->set('body', [
      [
        'bundle' => 'page',
        'langcode' => 'en',
        'value' => $data['body_value'],
        'summary' => strip_tags($data['body_summary']),
        'format' => 'full_html',
      ],
    ]);

    $entity->save();

    return $entity;
  }

}
